#! /bin/bash
#--------------------------------------
# ROUTINE FOR DETERMINING THE GROUND STATE
# LATTICE VECTOR FOR HCP MATERIAL
# Takes 3 arguments, 1 is mandatory:
# 1st arg lattice parameter a
# 2nd arg Name of Species, for Comment (Default X)
# 3rd arg Starting Ratio of c/a (Default 1.632993161855 ideal)
# Attempts to Build KPOINTS File Automatically if Specified Divisor > 0
#
# Date:   14th May 2015
# Author: Nicholas Hamilton
# Email:  n.hamilton@unsw.edu.au
#--------------------------------------

#Check the lattice vector value has been provided
if [ $# -lt 1 ]; then
    echo "Please Provide the Target Lattice Parameter for A (1st argument) "
    exit 0
fi

#VARIABLES
BIN="mpirun vasp"   			# The Executable
A=$1                			# Target Lattice Vector A (B == A in HCP)
dSTEP=0.1           			# Change in the lattice parameter a between steps
NSTEP=5             			# Number of Steps Either Side
SPEC=${2:-X}        			# The Default Species Name
RC2A=${3:-"1.632993161855"} 		# Target Lattice Vector Ratio C/A
dRAT=0.05                      	 	# Change in Ratio of C to A between steps in loop 2
OUTPUT=SUMMARY.hcp  			# The Output File
KPOINTDIVISOR=11                        # The Target Division in KPOINTS
BINKPOINTS=script_kpoints_automatic.R   # The Script to Build the KPOINTS FILE

#Determine the Steps
VA2A=$(awk -v c="$A"    -v n="$NSTEP" -v s="$dSTEP" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
VC2A=$(awk -v c="$RC2A" -v n="$NSTEP" -v s="$dRAT"  'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')

#Remove Wavcar file if it exists
if [ -f WAVCAR ]; then rm WAVECAR; fi

#Count the Successes and Failures
CSUC=0; CTOT=0

#Process the loop for each lattice vector
for a2a in ${VA2A}; do
for c2a in ${VC2A}; do
echo "Processing Lattice Vector: a=$a2a, c2a=$c2a" 

#Build the POSCAR
cat > POSCAR <<!
$SPEC fcc:
$a2a
 1.00000000000000 0.00000000000000 0.00000000000000
-0.50000000000000 0.86602540378444 0.00000000000000
 0.00000000000000 0.00000000000000 $c2a
$SPEC
2
Direct
 0.00000000000000 0.00000000000000 0.00000000000000
 0.33333333333333 0.66666666666667 0.50000000000000
!

#Build the KPOINTS File
if [ "$KPOINTDIVISOR" -gt "0" ]; then
        $BINKPOINTS -i POSCAR -o KPOINTS -d $KPOINTDIVISOR
fi

#Check KPOINTSS FILE EXISTS (Either Provided or Built)
if [ ! -f KPOINTS ]; then
	echo "KPOINTS File Doesn't Exist, Terminating..."
	exit 0
fi

#Run the Executable
$BIN

#Determine the Result and report
touch $OUTPUT
if [ -f OSZICAR ]; then
    E=$(tail -1 OSZICAR)
    echo $a2a $c2a $E >> $OUTPUT
    CSUC=$(($CSUC + 1))
fi
CTOT=$(($CTOT + 1))
done
done

#Report the Output File for Further Processing
if [ "$CSUC" -gt "0" ]; then
    echo Number of Values Successfully Processed: $CSUC/$CTOT
    cat $OUTPUT
else
    echo "All $CTOT Trials Failed to Process"
fi
exit 1
