#! /bin/bash
#--------------------------------------
# Routine for determining the ground state
# lattice parameters for the 14 bravis lattices
# in the 7 lattice families...
#
# Relies on two (2) external R scripts
#  1. Script for producing KPOINTS file
#  2. Script for producing bravis (unit) cell, 
#     given lattice parameters as input variables
# 
# Primative cells have been used where possible, 
# although not necessarily obvious -- See the FCC or 
# FC-Orthorhombic cases.
#
# Note, the required parameters depend on the
# intended bravis lattice, errors will be thrown
# if the exact number of parameters are not provided
# on a case-by-case basis.
# 
# Author: Nicholas Hamilton
# Email:  n.hamilton@unsw.edu.au
#--------------------------------------
# Usage:  script_volscan_all.sh SYSTEM SPECIES [parms: a bona cona alpha beta gamma]

#Check the lattice vector value has been provided
if [ $# -lt 2 ]; then echo "Please Provide (at a minimum) System and Species as the first two (2) arguments..."; exit 0; fi

#External Script Dependencies
BIN="mpirun vasp"                       # The Main Executable
BINKPOINTS=script_kpoints_automatic.R   # The Script to Build the KPOINTS FILE
BINBRAVIS=script_bravis.R               # The Script to Determine Cell

#VARIABLES
SYSTEM=$1                               # The Crystallographic System
SPEC=$2                                 # The Default Species Name to Include in POSCAR
OUTPUT=SUMMARY 				# The Output File
KPOINTDIVISOR=11			# The Target Division in KPOINTS

#Variables to do with the resulting mesh
stepVectorSize=0.20			# Step Magnitude for a, b, c
stepVectorRatio=0.10			# Step Magnitude for bona and cona
stepAngleDegrees=2.5			# Step Magnitude for alpha, beta, gamma in degrees
stepNumber=5				# The Number of Steps Either Side

#Function to build and/or check if the KPOINTS file exists
function doKPoints {
	#Build the kpoints file if possible
	if [ "$1" -gt "0" ]; then 
		$BINKPOINTS -i POSCAR -o KPOINTS -d $1 
	fi
	#Check if the kpoints file exists
	if [ ! -f KPOINTS ]; then
        	echo "KPOINTS File Doesn't Exist, Terminating..."
        	exit 0
	fi
}
#Function ro remove file if it exists
function removeFile {
	if [ -f $1 ]; then rm $1; fi
}
#function to remove poscar file if it exists
function rmPoscar {
	removeFile POSCAR
}
#Do some preparations, remove unnecessary files
function removeExistingFiles {
	removeFile WAVCAR
	removeFile $OUTPUT
	rmPoscar
}
#Function to store result
function storeResult {
	#Ensure file is created
        touch $OUTPUT
	#Send result to file
	echo $1 >> $OUTPUT
}

#Function to put the results to file, Expects 8 arguments, Outputfile a, b/a, c/a, alpha, beta, gamma, Energy
function interpretAndStoreResult {
	#Determine the Result and report
	if [ -f OSZICAR ]; then
	        energyLine=$(tail -1 OSZICAR)
		echo "Attempting to extract: $energyLine"
		energy=$(echo $energyLine | awk '{print $5}')
		#Check if result is a number
		if [[ "$energy" =~ ^[-+]*[0-9]*[.,]*[0-9]*[eE][-+][0-9]*$ ]]; then
    			echo "Energy resolved as number to: $energy"
		else
			echo "Energy ($energy) NOT resolved as number..."
			energy=NA
		fi
		#a bona cona alpha beta gamma energy
	else
		energy=NA
	fi
	#a bona cona alpha beta gamma energy
	storeResult "$1 $2 $3 $4 $5 $6 $energy"
	
}

#Function is number
isNumber() { 
	re='^-?[0-9]+([.][0-9]+)?$'
	if [[ $1 =~ $re ]]; then return 0; else return 1; fi
}
isInteger() {
	re='^-?[0-9]+$'
	if [[ $1 =~ $re ]]; then return 0; else return 1; fi
}

contains() {
	array=($1)
	for i in "${array[@]}"; do
    		if [[ $i == $2 ]]; then
        		return 0
    		fi
	done
	return 1
}

#Function to add extension to the result file
addExtensionToResult() {
	if [ -f $OUTPUT ]; then
		mv $OUTPUT "${OUTPUT}_${1}"
	else
		echo "$OUTPUT doesn't exist"
	fi
}

#$1=value, $2 is min number $3 is max number $4 is list
checkNumberOrRange() {
	if isNumber $1; then
		if [[ "$1" -ge "$2" && "$1" -le "$3" ]]; then
			return 0
		fi
	elif contains "$4" $1; then
		return 0
	fi
	return 1
}

#Function to add line to poscar file
lineToPoscar() { 
	echo $1 >> POSCAR 
}

#Add direct line to poscar
direct() { 
	lineToPoscar "Direct" 
}
#Add cartesian line to poscar
cartesian() { 
	lineToPoscar "Cartesian" 
}

#Add the number of atoms to poscar
atoms() { 
	if isInteger $1; then 
		lineToPoscar $1 
	fi
}

#Do preparation
removeExistingFiles

#Create Header Row in Result File
storeResult "a bona cona alpha beta gamma energy"

#--------------------------------------------------------
# TRICLINIC FAMILY
#--------------------------------------------------------
#[14/14] PRIMATIVE TRICLINIC
if checkNumberOrRange $1 1 2 "P1 P-1"; then
        extension="unknown" #Default
	echo "Processing Triclinic ($1) Volume Scan"
        parmNReq=8
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "Triclinic ($1) Requires 'a','b','c','alpha','beta' and 'gamma' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"; exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio"  'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio"  'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VALP=$(awk -v c="$6" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VBET=$(awk -v c="$7" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VGAM=$(awk -v c="$8" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        cnt=0
	for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                for alpha in ${VALP}; do
                                        for beta in ${VBET}; do
                                                for gamma in ${VGAM}; do
                                                        if awk "BEGIN {exit $alpha <= 180.0 && $alpha > 0 && $beta <= 180.0 && $beta > 0 && $gamma <= 180.0 && $gamma > 0 ? 0 : 1}"; then
                                                                echo "Processing Lattice Parameters: a=$a2a, bona=$b2a, cona=$c2a, alpha=$alpha, beta=$beta and gamma=$gamma" 
                                                                rmPoscar
                                                                cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --alpha $alpha --beta $beta --gamma $gamma --system triclinic)
                                                                lineToPoscar "$SPEC $extension cell:"
								lineToPoscar "$a2a"
								lineToPoscar "$cell"
								lineToPoscar "$SPEC"
								if [[ $1 -eq 1 || $1 == "P1" ]]; then
									atoms 1
									direct
									lineToPoscar "0.0 0.0 0.0"
									extension="P1"	
       	 							elif [[ $1 -eq 2 || $1 == "P-1" ]]; then
									atoms 2
									lineToPoscar " 1.0  1.0  1.0"
									lineToPoscar " -1.0 0.0  0.0"
									extension="P-1"
        							fi
                                                                doKPoints $KPOINTDIVISOR
                                                                $BIN
                                                                interpretAndStoreResult $a2a $b2a $c2a $alpha $beta $gamma
								cnt=$(($cnt+1))
                                                        fi
                                                done
                                        done
                                done
                        done
                done
        done
	if [[ $cnt -gt 0 ]]; then
        	addExtensionToResult $extension
	fi
fi



echo "Processing Script"






#-------------------------------------------------------
# HEXAGONAL FAMILY
#--------------------------------------------------------
#[01/14] CPH/CLOSE PACKED HEXAGONAL
if [ "$1" == "closepackedhexagonal" -o "$1" == 'hexagonal' -o "$1" == 'hcp' -o "$1" == "cph" ]; then
        extension=HCP
	echo "Processing $extension Volume Scan"
        parmNReq=4
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for c2a in ${VC2A}; do
			echo "Processing Lattice Parameters: a=$a2a, c2a=$c2a" 
			rmPoscar
			cell=$($BINBRAVIS -a 1.0 --cona $c2a --system hexagonal)
                	echo "$SPEC $extension cell:" 	>> POSCAR
                	echo "$a2a" 			>> POSCAR
                	echo "$cell" 			>> POSCAR
                	echo "$SPEC" 			>> POSCAR
                	echo "2" 			>> POSCAR
                	echo "Direct" 			>> POSCAR
                	echo "0.00000000000000 0.00000000000000 0.00000000000000" >> POSCAR
			echo "0.33333333333333 0.66666666666667 0.50000000000000" >> POSCAR
                        doKPoints $KPOINTDIVISOR
                        $BIN
                        interpretAndStoreResult $a2a 1 $c2a 90 90 120
                done
        done
        addExtensionToResult $extension


#--------------------------------------------------------
# CUBIC FAMILY
#--------------------------------------------------------
#[02/14] SC/PRIMATIVE CUBIC
elif [ "$1" == "primativecubic" -o "$1" == "cubic" -o "$1" == "cP" -o "$1" == "simplecubic" -o "$1" == "sc" ]; then
	extension=SC
        echo "Processing $extension Volume Scan"
        parmNReq=3
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VALS=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in $VALS ; do
                echo "Processing Lattice Parameters: a= $a2a" 
		rmPoscar
		cell=$($BINBRAVIS -a 1.0 --system cubic)
		echo "$SPEC $extension cell:" 	>> POSCAR
		echo "$a2a" 			>> POSCAR
		echo "$cell"			>> POSCAR
		echo "$SPEC" 			>> POSCAR
		echo "1" 			>> POSCAR
		echo "Direct" 			>> POSCAR
		echo "0.0 0.0 0.0" 		>> POSCAR
                doKPoints $KPOINTDIVISOR
                $BIN
		interpretAndStoreResult $a2a 1 1 90 90 90
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[03/14a] FCC/FACE CENTERED CUBIC (THIS IS PRIMATIVE FORM OF FCC)
elif [ "$1" == "facecenteredcubic" -o "$1" == "fcc" -o "$1" == "cF" ];then
	extension=FCC
	echo "Processing $extension Volume Scan"
	parmNReq=3
	if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
        	echo "$extension Requires 'a' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
        	exit 0
	fi
	VALS=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	for a2a in $VALS ; do
		echo "Processing Lattice Parameters: a= $a2a" 
		rmPoscar
		echo "$SPEC $extension cell"	>> POSCAR
		echo "$a2a" 			>> POSCAR
		echo "0.0 0.5 0.5" 		>> POSCAR
		echo "0.5 0.0 0.5" 		>> POSCAR
		echo "0.5 0.5 0.0" 		>> POSCAR
		echo "$SPEC" 			>> POSCAR
		echo "1" 			>> POSCAR
		echo "Direct" 			>> POSCAR
		echo "0.0 0.0 0.0" 		>> POSCAR
		doKPoints $KPOINTDIVISOR
		$BIN
		interpretAndStoreResult $a2a 1 1 90 90 90
	done
	addExtensionToResult $extension
#--------------------------------------------------------
#[03/14b] FCC/DIAMOND CUBIC
elif [ "$1" == "diamondcubic" -o "$1" == "dc" -o $1 == "Fd-3m" ];then
        extension=DC
        echo "Processing $extension Volume Scan"
        parmNReq=3
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VALS=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in $VALS ; do
                echo "Processing Lattice Parameters: a= $a2a" 
                rmPoscar
                echo "$SPEC $extension cell"    >> POSCAR
                echo "$a2a"                     >> POSCAR
                echo "0.0 0.5 0.5"              >> POSCAR
                echo "0.5 0.0 0.5"              >> POSCAR
                echo "0.5 0.5 0.0"              >> POSCAR
                echo "$SPEC"                    >> POSCAR
                echo "2"                        >> POSCAR
                echo "Direct"                   >> POSCAR
                echo "0.0 0.0 0.0" 		>> POSCAR
		echo "0.5 0.5 0.5"		>> POSCAR
		doKPoints $KPOINTDIVISOR
                $BIN
                interpretAndStoreResult $a2a 1 1 90 90 90
        done
        addExtensionToResult $extension

#[141/230]
elif [ "$1" == "141" -o "$1" == "I41/amd" -o "$1" == "I41/a2/m2/d" ];then
        extension=141
        echo "Processing Tetragonal (No. $extension) Volume Scan"
        parmNReq=4
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        A2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize"  'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        C2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	for a2a in $A2A ; do
		for c2a in $C2A; do
                	echo "Processing Lattice Parameters: a= $a2a, ctoa=$c2a" 
                	rmPoscar
			lineToPoscar "$SPEC $extension cell"
			lineToPoscar "$a2a"
			$BINBRAVIS -a 1.0 --cona $c2a --system tetragonal >> POSCAR
			lineToPoscar "$SPEC"
			lineToPoscar 4
			direct
			lineToPoscar "0.00 0.00 0.00"
			lineToPoscar "0.50 0.50 0.50"
			lineToPoscar "0.00 0.50 0.25"
			lineToPoscar "0.50 0.00 0.75"
			doKPoints $KPOINTDIVISOR
                	$BIN
                	interpretAndStoreResult $a2a 1 $c2a 90 90 90
		done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[04/14] BCC/BODY CENTERED CUBIC
elif [ "$1" == "bodycenteredcubic" -o "$1" == "bcc" -o "$1" == "cl" ]; then
        extension=BCC
	echo "Processing $extension Volume Scan"
        parmNReq=3
	if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VALS=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}') 
        for a2a in $VALS ; do
                echo "Processing Lattice Parameters: a=$a2a" 
                rmPoscar
		cell=$($BINBRAVIS -a 1.0 --system cubic)
		echo "$SPEC $extension cell:" 	>> POSCAR
                echo "$a2a" 			>> POSCAR
                echo "$cell"			>> POSCAR
		echo "$SPEC" 			>> POSCAR
                echo "2" 			>> POSCAR
                echo "Direct" 			>> POSCAR
                echo "0.0 0.0 0.0" 		>> POSCAR
		echo "0.5 0.5 0.5" 		>> POSCAR
                doKPoints $KPOINTDIVISOR
                $BIN
                interpretAndStoreResult $a2a 1 1 90 90 90
        done
        addExtensionToResult $extension

#--------------------------------------------------------
# RHOMBOHEDRAL FAMILY, ALSO KNOWN AS TRIGONAL
#--------------------------------------------------------
#[05/14] Primative Rhombohedral
elif [ "$1" == "primativerhombohedral" -o "$1" == 'rhombohedral' -o "$1" == 'rhb' -o "$1" == 'trigonal' ]; then
	extension=RHB
        echo "Processing $extension Volume Scan"
        parmNReq=4
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' and 'alpha' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VALP=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for alpha in ${VALP}; do
			if awk "BEGIN {exit $alpha <= 90.0 && $alpha > 0 ? 0 : 1}"; then
                        	echo "Processing Lattice Parameters: a=$a2a, alpha=$alpha" 
				rmPoscar
				cell=$($BINBRAVIS -a 1.0 --bona 1.0 --cona 1.0 --alpha $alpha --system rhombohedral)
				echo "$SPEC $extension cell:" 	>> POSCAR
				echo "$a2a" 			>> POSCAR
				echo "$cell"			>> POSCAR
				echo "$SPEC" 			>> POSCAR
				echo "1" 			>> POSCAR
				echo "Direct" 			>> POSCAR
				echo "0.0 0.0 0.0" 		>> POSCAR
                        	doKPoints $KPOINTDIVISOR
                        	$BIN
                        	interpretAndStoreResult $a2a 1 1 $alpha $alpha $alpha
			fi
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
# TETRAGONAL FAMILY
#--------------------------------------------------------
#[06/14] PTET/PRIMATIVE TETRAGONAL
elif [ "$1" == 'primativetetragonal' -o "$1" == "tetragonal" -o "$1" == 'ptet' ]; then
	extension=PTET
        echo "Processing $extension Volume Scan"
        parmNReq=4
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for c2a in ${VC2A}; do
                        echo "Processing Lattice Parameters: a=$a2a, c2a=$c2a" 
			rmPoscar
			cell=$($BINBRAVIS -a 1.0 --cona $c2a --system tetragonal)
			echo "$SPEC $extension cell:" 	>> POSCAR
			echo "$a2a" 			>> POSCAR
			echo "$cell" 			>> POSCAR
			echo "$SPEC" 			>> POSCAR
			echo "1" 			>> POSCAR
			echo "Direct" 			>> POSCAR
			echo "0.0 0.0 0.0" 		>> POSCAR
                        doKPoints $KPOINTDIVISOR
                        $BIN
                        interpretAndStoreResult $a2a 1 $c2a 90 90 90
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[07/14] BCTET/BODY CENTERED TETRAGONAL
elif [ "$1" == 'bodycenteredtetragonal' -o "$1" == 'bctet' ]; then
	extension=BCTET
        echo "Processing $extension Volume Scan"
        parmNReq=4
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for c2a in ${VC2A}; do
                        echo "Processing Lattice Parameters: a=$a2a, c2a=$c2a" 
                        rmPoscar
			cell=$($BINBRAVIS -a 1.0 --cona $c2a --system tetragonal)
			echo "$SPEC $extension cell:" 	>> POSCAR
                        echo "$a2a" 			>> POSCAR
			echo "$cell" 			>> POSCAR
                        echo "$SPEC" 			>> POSCAR
                        echo "2" 			>> POSCAR
                        echo "Direct" 			>> POSCAR
                        echo "0.0 0.0 0.0" 		>> POSCAR
			echo "0.5 0.5 0.5" 		>> POSCAR
                        doKPoints $KPOINTDIVISOR
                        $BIN
                        interpretAndStoreResult $a2a 1 $c2a 90 90 90
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
# ORTHORHOMBIC FAMILY
#--------------------------------------------------------
#[08/14] PRIMATIVE ORTHORHOMBIC
elif [ "$1" == 'primativeorthorhombic' -o "$1" == "orthorhombic" -o "$1" == 'port' ]; then
	extension=PORT
        echo "Processing $extension Volume Scan"
        parmNReq=5
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','bona' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
			for c2a in ${VC2A}; do
                        	echo "Processing Lattice Parameters: a=$a2a, b2a=$b2a and c2a=$c2a" 
                        	rmPoscar
                        	cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --system orthorhombic)
				echo "$SPEC $extension cell:" 	>> POSCAR
                        	echo "$a2a" 			>> POSCAR
				echo "$cell"			>> POSCAR
                        	echo "$SPEC" 			>> POSCAR
                        	echo "1" 			>> POSCAR
                        	echo "Direct" 			>> POSCAR
                        	echo "0.0 0.0 0.0" 		>> POSCAR
                        	doKPoints $KPOINTDIVISOR
                        	$BIN
                        	interpretAndStoreResult $a2a $b2a $c2a 90 90 90
                	done
		done
        done
        addExtensionToResult $extension

#[64/230]
elif [ "$1" == "64" -o "$1" == "Cmca" -o "$1" == "C2/m2/c21/a" ]; then
 	extension=64
        echo "Processing $extension Volume Scan"
        parmNReq=5
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "Orthorhombic ($extension) Requires 'a','bona' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                echo "Processing Lattice Parameters: a=$a2a, b2a=$b2a and c2a=$c2a" 
                                rmPoscar
                                echo "$SPEC $extension cell:"   >> POSCAR
                                echo "$a2a"                     >> POSCAR
                                $BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --system orthorhombic >> POSCAR
                                echo "$SPEC"                    >> POSCAR
                                echo "8"                        >> POSCAR
                                echo "Direct"                   >> POSCAR
                                echo "0.0 0.0 0.0"              >> POSCAR
				doKPoints $KPOINTDIVISOR
                                $BIN
                                interpretAndStoreResult $a2a $b2a $c2a 90 90 90
                        done
                done
        done
        addExtensionToResult $extension

#[64/230 for Gallium]
elif [ "$1" == "Gallium" -o "$1" == "gallium" ]; then
        extension=64
        echo "Processing $extension Volume Scan"
        parmNReq=5
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "Orthorhombic ($extension) Requires 'a','bona' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
 	removeFile $OUTPUT
	storeResult "a bona cona alpha beta u v gamma energy"	
        VA2A=$(awk -v c="$3"     -v n="4" -v s="$stepVectorSize"  	'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4"     -v n="4" -v s="$stepVectorRatio" 	'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5"     -v n="4" -v s="$stepVectorRatio" 	'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	LPMU=$(awk -v c="0.0785" -v n="2" -v s="0.025"  		'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	LPMV=$(awk -v c="0.1525" -v n="2" -v s="0.050"                  'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
				for u in $LPMU; do
					for v in $LPMV; do
                                		echo "Processing Lattice Parameters: a=$a2a, b2a=$b2a, c2a=$c2a, u=$u, v=$v" 
                                		va=$(echo "$v" | awk '{print 0.5 - $1}')
                                                vb=$(echo "$v" | awk '{print 0.5 + $1}')
                                                vc=$(echo "$v" | awk '{print 1.0 - $1}')
                                                ua=$(echo "$u" | awk '{print 0.5 + $1}')
                                                ub=$(echo "$u" | awk '{print 0.5 - $1}')
                                                uc=$(echo "$u" | awk '{print 1.0 - $1}')
						rmPoscar
                                		echo "$SPEC $extension cell:"   >> POSCAR
                                		echo "$a2a"                     >> POSCAR
                                		$BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --system orthorhombic >> POSCAR
                                		echo "$SPEC"                    >> POSCAR
                                		echo "8"                        >> POSCAR
                                		echo "Direct"                   >> POSCAR
						echo "0.0000 $v  $u" 		>> POSCAR #    x,    y,    z
  						echo "0.0000 $va $ua"		>> POSCAR #    x,1/2-y,1/2+z
						echo "0.0000 $vb $ub"		>> POSCAR #    x,1/2+y,1/2-z
						echo "0.0000 $vc $uc"		>> POSCAR #    x,   -y,   -z
						echo "0.5000 $vb $u"		>> POSCAR #1/2+x,1/2+y,   +z
						echo "0.5000 $vc $ua"		>> POSCAR #1/2+x,   -y,1/2+z
						echo "0.5000 $v  $ub"		>> POSCAR #1/2+x,   +y,1/2-z
						echo "0.5000 $va $uc"		>> POSCAR #1/2+x,1/2-y,   -z  
						doKPoints $KPOINTDIVISOR
                                		$BIN
                                		interpretAndStoreResult $a2a $b2a "$c2a $u $v" 90 90 90
					done
				done
                        done
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[09/14] BODY CENTERED ORTHORHOMBIC
elif [ "$1" == 'bodycenteredorthorhombic' -o "$1" == 'bcort' ]; then
	extension=BCORT
        echo "Processing $extension Volume Scan"
        parmNReq=5
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','bona' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                echo "Processing Lattice Parameters: a=$a2a, b2a=$b2a, c2a=$c2a" 
                                rmPoscar
                                cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --system orthorhombic)
				echo "$SPEC $extension cell:" 	>> POSCAR
                                echo "$a2a" 			>> POSCAR
                                echo "$cell"			>> POSCAR
                                echo "$SPEC" 			>> POSCAR
                                echo "2" 			>> POSCAR
                                echo "Direct" 			>> POSCAR
                                echo "0.0 0.0 0.0" 		>> POSCAR
                                echo "0.5 0.5 0.5" 		>> POSCAR
				doKPoints $KPOINTDIVISOR
                                $BIN
                                interpretAndStoreResult $a2a $b2a $c2a 90 90 90
                        done
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[10/14] BASE CENTERED ORTHORHOMBIC
elif [ "$1" == 'basecenteredorthorhombic' -o "$1" == 'bscort' ]; then
        extension=BSCORT
	echo "Processing $extension Volume Scan"
        parmNReq=5
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','bona' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                echo "Processing Lattice Parameters: a=$a2a, c2a=$c2a" 
                                rmPoscar
                                cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --system orthorhombic)
				echo "$SPEC $extension cell:" 	>> POSCAR
                                echo "$a2a" 			>> POSCAR
				echo "$cell"			>> POSCAR
                                echo "$SPEC" 			>> POSCAR
                                echo "2" 			>> POSCAR
                                echo "Direct" 			>> POSCAR
                                echo "0.0 0.0 0.0" 		>> POSCAR
                                echo "0.5 0.5 0.0" 		>> POSCAR
				doKPoints $KPOINTDIVISOR
                                $BIN
                                interpretAndStoreResult $a2a $b2a $c2a 90 90 90
                        done
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[11/14] FACE CENTERED ORTHORHOMBIC (THIS IS PRIMATIVE FORM OF FC ORTHORHOMBIC)
elif [ "$1" == 'facecenteredorthorhombic' -o "$1" == 'fcort' ]; then
	extension=FCORT
        echo "Processing $extension Volume Scan"
        parmNReq=5
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','bona' and 'cona' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                echo "Processing Lattice Parameters: a=$a2a, c2a=$c2a" 
                                rmPoscar
				b2a2=$(awk "BEGIN{print $b2a/2.0}")
				c2a2=$(awk "BEGIN{print $c2a/2.0}")
                                echo "$SPEC $extension cell:" 	>> POSCAR
                                echo "$a2a" 			>> POSCAR
                                echo "0.0 $b2a2 $c2a2" 		>> POSCAR
				echo "1.0 0.0 $c2a2"  		>> POSCAR
				echo "1.0 $b2a2 0.0" 		>> POSCAR
				echo "$SPEC" 			>> POSCAR
				echo "1" 			>> POSCAR
				echo "Direct" 			>> POSCAR
				echo "0.0 0.0 0.0" 		>> POSCAR
                                doKPoints $KPOINTDIVISOR
                                $BIN
                                interpretAndStoreResult $a2a $b2a $c2a 90 90 90
                        done
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
# MONONCLINIC FAMILY
#--------------------------------------------------------
#[12/14] PRIMATIVE MONOCLINIC
elif [ "$1" == 'primativemonoclinic' -o "$1" == "monoclinic" -o "$1" == 'pmc' ]; then
	extension=PMC
        echo "Processing $extension Volume Scan"
        parmNReq=6
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','b','c' and 'beta' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VBET=$(awk -v c="$6" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
		for b2a in ${VB2A}; do
			for c2a in ${VC2A}; do
                		for beta in ${VBET}; do
                        		if awk "BEGIN {exit $beta <= 90.0 && $beta > 0 ? 0 : 1}"; then
                                		echo "Processing Lattice Parameters: a=$a2a, bona=$b2a, cona=$c2a, beta=$beta" 
                                		rmPoscar
                                		cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --beta $beta --system monoclinic)
						echo "$SPEC $extension cell:" 	>> POSCAR
                                		echo "$a2a" 			>> POSCAR
						echo "$cell"			>> POSCAR
                                		echo "$SPEC" 			>> POSCAR
                                		echo "1" 			>> POSCAR
                                		echo "Direct" 			>> POSCAR
                                		echo "0.0 0.0 0.0" 		>> POSCAR
                                		doKPoints $KPOINTDIVISOR
                                		$BIN
                                		interpretAndStoreResult $a2a $b2a $c2a 90 $beta 90
                        		fi
				done
			done
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#[13/14] BASE CENTERED MONOCLINIC
elif [ "$1" == 'basecenteredmonoclinic' -o "$1" == 'bscmc' ]; then
        extension=BSCMC
	echo "Processing $extension Volume Scan"
        parmNReq=6
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','b','c' and 'beta' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"
                exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VBET=$(awk -v c="$6" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                for beta in ${VBET}; do
                                        if awk "BEGIN {exit $beta <= 90.0 && $beta > 0 ? 0 : 1}"; then
                                                echo "Processing Lattice Parameters: a=$a2a, bona=$b2a, cona=$c2a, beta=$beta" 
                                                rmPoscar
                                                cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --beta $beta --system monoclinic)
						echo "$SPEC $extension cell:" 	>> POSCAR
                                                echo "$a2a" 			>> POSCAR
						echo "$cell"			>> POSCAR
                                                echo "$SPEC" 			>> POSCAR
                                                echo "2" 			>> POSCAR
                                                echo "Direct" 			>> POSCAR
                                                echo "0.0 0.0 0.0" 		>> POSCAR
						echo "0.5 0.5 0.0" 		>> POSCAR
                                                doKPoints $KPOINTDIVISOR
                                                $BIN
                                                interpretAndStoreResult $a2a $b2a $c2a 90 $beta 90
                                        fi
                                done
                        done
                done
        done
	addExtensionToResult $extension

#--------------------------------------------------------
# TRICLINIC FAMILY
#--------------------------------------------------------
#[14/14] PRIMATIVE TRICLINIC
elif [ "$1" == 'primativetriclinic' -o "$1" == 'triclinic' -o "$1" == "tcl" ]; then
        extension=TCL
        echo "Processing $extension Volume Scan"
        parmNReq=8
        if [ $# -lt $parmNReq -o $# -gt $parmNReq ]; then
                echo "$extension Requires 'a','b','c','alpha','beta' and 'gamma' lattice parameter, you have provided $# parameters to this script, we require $parmNReq"; exit 0
        fi
        VA2A=$(awk -v c="$3" -v n="$stepNumber" -v s="$stepVectorSize"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VB2A=$(awk -v c="$4" -v n="$stepNumber" -v s="$stepVectorRatio"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VC2A=$(awk -v c="$5" -v n="$stepNumber" -v s="$stepVectorRatio"   'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	VALP=$(awk -v c="$6" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        VBET=$(awk -v c="$7" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
	VGAM=$(awk -v c="$8" -v n="$stepNumber" -v s="$stepAngleDegrees" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')
        for a2a in ${VA2A}; do
                for b2a in ${VB2A}; do
                        for c2a in ${VC2A}; do
                                for alpha in ${VALP}; do
					for beta in ${VBET}; do
						for gamma in ${VGAM}; do
                                        		if awk "BEGIN {exit $alpha <= 90.0 && $alpha > 0 && $beta <= 90.0 && $beta > 0 && $gamma <= 90.0 && $gamma > 0 ? 0 : 1}"; then
                                                		echo "Processing Lattice Parameters: a=$a2a, bona=$b2a, cona=$c2a, alpha=$alpha, beta=$beta and gamma=$gamma" 
                                                		rmPoscar
                                                		cell=$($BINBRAVIS -a 1.0 --bona $b2a --cona $c2a --alpha $alpha --beta $beta --gamma $gamma --system triclinic)
								echo "$SPEC $extension cell:" 	>> POSCAR
                                                		echo "$a2a" 			>> POSCAR
								echo "$cell"			>> POSCAR
                                                		echo "$SPEC" 			>> POSCAR
                                                		echo "1" 			>> POSCAR
                                                		echo "Direct" 			>> POSCAR
                                                		echo "0.0 0.0 0.0" 		>> POSCAR
                                                		doKPoints $KPOINTDIVISOR
                                                		$BIN
                                                		interpretAndStoreResult $a2a $b2a $c2a $alpha $beta $gamma
                                        		fi
						done
					done
                                done
                        done
                done
        done
        addExtensionToResult $extension

#--------------------------------------------------------
#OTHERS AREN'T IDENTIFIED
else 
	echo "$1 is an Invalid system, please check the input argument..."
	exit 0
fi

#Success
exit 1
