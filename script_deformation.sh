#!/bin/bash

#--------------------------------------
# ROUTINE FOR PRODUCING A FAMILIY OF 
# DEFORMATIONS, FOR PRODUCING FILESETS
# USED IN RELAXATION FOR CALCLATING 
# ELASTIC CONSTANTS
#
# ALSO PLACES A SERIES OF STATIC FILES
# IN EACH OF THE DESTINATION DIRECTORIES
# FOR EACH OF THE DEFOFMATION SETS
#
# Date:   28th April 2015
# Author: Nicholas Hamilton
# Email:  n.hamilton@student.unsw.edu.au
#--------------------------------------

#SCRIPT TO PRODUCE DISTORTION FILES, WHEN DETERMINING ELASIC CONSTANTS
INPUT="POSCAR"
OUTPUT_DIR="1_DISTORTION"
TYPES="dV dXY dXZ dYZ"
STRAINS="+0.010 -0.010 +0.020 -0.020 +0.030 -0.030"
STATIC_FILES="INCAR POTCAR KPOINTS"
PBS_FILE="PBS"

#Calculation Parms
EXE="va++"      #CPP APPLICATION FOR EXECUTING THE DISTORTION
CALC="distort"  #CALCULATION TYPE IN CPP APPLICATION

#Determine the Alloy
if [ $# -eq 0 ];then
	echo "Expecting 1st Argument which was missing, Using 'EC' for alloy code instead..."
	sleep 5;
	ALLOY="EC"
else
	ALLOY="$1"
fi

#Function to process the static files
#arg1 = target directory
function processStaticFiles(){
        for f in ${STATIC_FILES}; do
                if [ -f $f ]; then
                        cp $f $1/$f
                else
                        echo "Static File $f Does Not Exist"
                fi
        done
}

#Function to process the PBS script
#arg1 = strain, arg2 = type, arg3= target directory
function processPBSScript(){
	PREFIX="#PBS -N "
	NEWLINE="${PREFIX}${ALLOY}_${2}_${1}"
        NEWLINE=${NEWLINE:0:$((${#PREFIX} + 15))}
	if [ -f $PBS_FILE ]; then
                cp $PBS_FILE $3/$PBS_FILE
		sed -i '' "2s/.*/${NEWLINE}/" $3/${PBS_FILE}
        else
                echo "File ${PBS_FILE} Does Not Exist"
        fi	
}

#Function to process the distortion
#arg1 = strain, arg2 = type, arg3= target directory
function processDistortion(){
	if [ ! -f $INPUT ];then
		echo "File ${INPUT} Does Not Exist, This is Required"
		exit 0
	fi

	#If Target Directory Exists
 	if [ -d $3 ];then
		#Run the Application to Distort the Input File
                $EXE -c $CALC -i $INPUT --$2 $1 -o "$3/$INPUT"

		#Rename to Input File in Destination Directory
                A="$3/${INPUT}_${CALC}"
                B="$3/${INPUT}"
                if [ -f $A ]; then
                        mv $A $B
                fi
        else
                echo "Directory ${3} Does Not Exist"
        fi
}


#Create the function to process
#arg1 = strain, arg2 = type, arg3 = strain number, arg4 = type number
function processIteration {

	#Replace Undesired Characters to produce standardised directory format
	S=$1
	S=${S//[+]/P}
	S=${S//[-]/M}
	S=${S//[.]/p}

	#Build the Output Directory Recursively
	TD="./${OUTPUT_DIR}/${4}_${2}/${3}_${S}"
	mkdir -p ${TD}

	#Do the Distortion
	processDistortion $1 $2 $TD

	#Copy the Static Files
	processStaticFiles $TD

	#Produce the PBS Script
	processPBSScript $S $2 $TD
}

#PROCESS LOOP
ITERATION_COUNT=0
function processLoop(){
	#Initial Strain Number
	NUMBER_STRAIN=1
	#Loop over all available strains
	for STRAIN in $STRAINS; do
		#Initial Number Type
		NUMBER_TYPE=1
		#Loop over all available types
		for TYPE in $TYPES; do
			#Process at this iteration
			processIteration $STRAIN $TYPE $NUMBER_STRAIN $NUMBER_TYPE	
			#Increment the Type Number
			NUMBER_TYPE=$(($NUMBER_TYPE + 1))
			ITERATION_COUNT=$(($ITERATION_COUNT + 1))
		done
		#Increment the Strain Number
		NUMBER_STRAIN=$(($NUMBER_STRAIN + 1))
	done
}

#Now Finally Execute
processLoop
echo "Finished Processing ${ITERATION_COUNT} Deformations"
