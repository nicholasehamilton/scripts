#!/bin/bash

#Variables
exeCmp="xelatex -synctex=1 -interaction=nonstopmode"
exeBib="biber"
exeIX="makeindex"
exeGlo="makeglossaries"

#Process the Script
if [ $# -gt 0 ]; then
	if [ -f $1.tex ]; then 
		$exeCmp -draftmode $1.tex; 	#Compile Round 1
		$exeBib $1;			#Bibliography
		$exeIX  $1.idx;			#Index
		$exeGlo $1			#Glossary
		$exeCmp -draftmode $1.tex; 	#Compile Round 2
		$exeCmp $1.tex;            	#Compile Round 3, FINAL
	else
		echo "File $1.tex Doesn't Exist"
	fi
else 
	echo "File name not specified"
fi
