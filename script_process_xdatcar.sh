#!/bin/bash

#CALCULATION FLAGS FOR DOING VARIOUS ROUTINES
#PAIR DISTRIBUTION FUNCTION
DOPDF=false
#BOND ANGLE DISTRIBUTION, TAKES MUCH OF TIME
DOBAD=false
#SHELL CUTTOFFS
DOSHL=false
#MEAN SQUARE DISPLACEMENT
DOMSD=false
#DIFFUSION COEFFICIENTS
DODFS=false
#COORDINATION NUMBERS
DOCN=true

#LIST OF FILES, FOR NVT and NPT Simulations
REGEX_NVT=$(find . -iregex './XDATCAR_[0-9][0-9][0-9][0-9]' | sort -n)
REGEX_NPT=${REGEX_NVT}

#VARIABLES/PARAMETERS TO PASS TO THE ROUTINES
#The Exacutable File
EXE="va++"
#THE TYPES TO CONSIDER: nvt or npt or both (space seperated)
TYPES="nvt"        
#DEFAULT OUTPUT FOLDER
OUTPUT_FOLDER="./Output"	
#THE COORDINATION NUMBERS TO PROCESS
SHELLS=(1 2 3)				
#PERIODIC BOUNDARY CONDITIONS FOR gofr
PBC=1					    
#THE ITERATIONS, 0 is unlimited
ITERATIONS=0				
#THE SMOOTHING PERIOD, > 1 indicates periods, < 1 indicates fraction of dataset
SMOOTHING=0.10				
#THE UNIT OF DISPLACEMENT
UDISPLACEMENT="1e-10"       
#THE NPT TIMESTEP
UTIMESTEP_NPT="2e-15"		
#THE NVT TIMESTEP
UTIMESTEP_NVT="5e-15"		
#THE GENERIC FLAGS
FLAGS="--dr 0.025 --reportevery 5 --regstart 0.1 --regfinish 0.60"  

#FUNCTIONS
function join(){ 
	local IFS="${1:-' '}"
	shift
	echo "$*"; 
}

#Notify Start of the Script
echo "Starting Batch Script"

#OUTER LOOP
for TYPE in $TYPES
do
	#CHECK OUTPUT FOLDER EXISTS
	echo "Creating Output Folder: ${OUTPUT_FOLDER}, if it doesn't exist"	
	mkdir -p ${OUTPUT_FOLDER}

	#CYCLE THROUGH NPT OR NVT
	if [[ $TYPE == 'npt' ]]; then
		BATCHES=$REXEX_NPT
		UTIMESTEP=${UTIMESTEP_NPT}
	else
		BATCHES=$REGEX_NVT
		UTIMESTEP=${UTIMESTEP_NVT}
	fi

	#REPORT THE FILES TO BE PROCESSED
	echo "Processing the Following Files:"
	echo $BATCHES

	#CYCLE THROUGH	
	for INPUT in $BATCHES
	do
		#THE BASE EXECUTABLE COMMAND
		BASE="$EXE -i $INPUT --type $TYPE -o ${OUTPUT_FOLDER}/${INPUT} --iterations $ITERATIONS $FLAGS"

		#CHECK THAT THE FILE EXISTS
		if [ ! -f $INPUT ]; then
			echo "File $INPUT Doesn't Exist, terminating..."
			exit 0
		else
			echo "File $INPUT Exists, Processing..."
		fi

		#PAIR DISTRIBUTION FUNCTIONS
		if $DOPDF; then
			echo "Calculating Pair Distribution Function"
			$BASE -c pdf --pbc $PBC
		fi

		#COORDINATION SHELLS CUTTOFF VALUES
		if $DOSHL || $DOBAD; then
			SHELLSSTR=$(join , "${SHELLS[@]}" )
			#SHELLSSTR=${SHELLSSTR//,/ }
			echo "Determining the Limits of the Coordination Shells"
			$BASE -c shells --shells $SHELLSSTR --pbc $PBC --smoothing $SMOOTHING
		fi


		#BOND ANGLE DISTRIBUTIONS
		if $DOBAD; then
			echo "Determining the Bond Angle Distribution"
			for shell in "${SHELLS[@]}"; do
				echo "Processing Shell #$shell"
				CMD=bad
				SUFFIX=shells_${shell}
		        	LIMITSFILE=${OUTPUT_FOLDER}/${INPUT}_${SUFFIX}
		        	$BASE -c $CMD --pbc 0 --rminmaxfile $LIMITSFILE
		        	OUTPUT=${INPUT}_${CMD}
	        		mv ${OUTPUT_FOLDER}/${OUTPUT} ${OUTPUT_FOLDER}/${OUTPUT}_${SUFFIX}
			done
		fi
	
		#MEAN SQUARE DISPLACEMENT
		if $DOMSD; then
			echo "Determining the Mean Square Distance"
			$BASE -c msd --pbc $PBC --utimestep $UTIMESTEP --udisplacement $UDISPLACEMENT
		fi

		#DIFFUSION
		if $DODFS; then
			echo "Determining the Diffusion Coefficients"
			$BASE -c D --pbc $PBC --utimestep $UTIMESTEP --udisplacement $UDISPLACEMENT
		fi

        	#COORDINATION NUMBERS
        	if $DOCN; then
            		echo "Determining the Coordination Numbers"
			for shell in "${SHELLS[@]}";do
            			#REPORT
				echo "Procesing Shell #${shell}"
				CMD=cn
				SUFFIX=shells_${shell}
				LIMITSFILE=${OUTPUT_FOLDER}/${INPUT}_${SUFFIX}
				$BASE -c $CMD --pbc $PBC --rminmaxfile $LIMITSFILE
				OUTPUT=${INPUT}_${CMD}
				mv ${OUTPUT_FOLDER}/${OUTPUT} ${OUTPUT_FOLDER}/${OUTPUT}_${SUFFIX}
			done
        	fi
		done
done
	
#DONE
echo "Done"


