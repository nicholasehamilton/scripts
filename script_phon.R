#!/usr/bin/env Rscript
#--------------------------------------
# Run the phon sequence to prepare
# displacement files for phonon calculations
#
# Date:   27th June 2015
# Author: Nicholas Hamilton
# Email:  n.hamilton@unsw.edu.au
#--------------------------------------

#Load the Packages, Install if Necessary via pacman library
if(!suppressMessages(require('pacman'))){install.packages('pacman',type='source');suppressMessages(library(pacman))}
p_load(c("optparse","plyr"),character.only=TRUE)

options( 'default.phon.executable' = 'phon',
         'default.phon.logfile'    = 'phon.log',
         'digits'                  = 15,     #Decimals in Output File
         'width'                   = 10000)  #Width of Tables printed to console

#Set the command line arguments
options_list = list(
  make_option(c('--alloy'),
              type="character",
              default='X',
              dest="alloy",
              help="The name of the alloy, for substitution into the PBS file [default %default]"),
  make_option(c("--displacements"),
              type    = 'logical',
              action  = 'store_true', 
              default  = FALSE,
              dest     = 'displacements',
              help    = "Produce the Displacement Batches [default '%default']"),
  make_option(c("--forcefile"),
              type    = 'logical',
              action  = 'store_true', 
              default  = FALSE,
              dest     = 'forcefile',
              help    = "Produce the Force File, from Previously Executed Displacement Batches, Requires the .schedule file [default '%default']"),
  make_option(c("--positive"),
              type    = 'logical',
              action  = 'store_true', 
              default  = FALSE,
              dest     = 'positive',
              help    = "Include the Positive Displacements [default '%default']
                NOTE: If both --positive and --negative are FALSE, then 
                      positive displacements will be used"),
  make_option(c("--negative"),
              type    = 'logical',
              action  = 'store_true', 
              default  = FALSE,
              dest     = 'negative',
              help    = "Include the Positive Displacements [default '%default']
                NOTE: If both --positive and --negative are FALSE, then 
                      positive displacements will be used"),
  make_option(c('--phonexe'),
              type="character",
              default=getOption("default.phon.executable"),
              dest="phonexe",
              help="The phon executable filename [default '%default']"),
  make_option(c('--phonlog'),
              type="character",
              default=getOption("default.phon.logfile"),
              dest="phonlog",
              help="The default phon logfilename [default '%default']"),
  make_option(c("-v", "--verbose"),
              type    = 'logical',
              action  = 'store_true', 
              default  = FALSE,
              dest     = 'verbose',
              help	  = "Print extra output [default '%default']")
)

#Parse the command options
parser <- OptionParser(usage = "%prog Options", option_list=options_list)
args   <- parse_args(parser,positional_arguments=TRUE)

#Variables
verbose          = args$options$verbose
ALLOY            = args$options$alloy
PHON_EXE         = args$options$phonexe
PHON_LOG         = args$options$phonlog
INCLUDE_POS      = args$options$positive
INCLUDE_NEG      = args$options$negative
FORCEFILE        = args$options$forcefile
DISPLACEMENTS    = args$options$displacements
FILES_REQ        = c("INPHON","POSCAR","KPOINTS","PBS","POTCAR","INCAR")
START_ROW_POS    = 9

#Run Some Checks.
if(file.exists("POSCAR.bup")){     stop("Backup file POSCAR.bup exists",call.=FALSE)}
if(!all(file.exists(FILES_REQ))){  stop(sprintf("'%s' Files are ALL Required",paste(FILES_REQ,collapse="','")),call.=FALSE) }
if(!xor(FORCEFILE,DISPLACEMENTS)){ stop("either --forcefile OR --displacements must be specified, but NOT both and NOT neither (xor)", call.=FALSE) }

#Helper function to remove files if they exist
removeIfExists <- function(x,verbose=TRUE){ 
  for(f in x){ 
    if(file.exists(f)){ 
      if(verbose){ writeLines(sprintf('Removing %s',f)) }; 
      file.remove(f) 
    }
  }
}

#Base Directory where the batch files are to be placed
baseDirectory    = "1_PHON_BATCH"
scheduleFile     = sprintf("%s.schedule",baseDirectory)
forceFile        = sprintf("%s.forces",  baseDirectory)

#Function to produce the displacement batches
doDisplacements <- function(){
  
  #Run Displacement Specific Check
  if(!INCLUDE_POS & !INCLUDE_NEG){ writeLines("Using Posiive Displacements, as both --positive and --negative are currently FALSE"); INCLUDE_POS = TRUE }
  
  #Remove existing output files
  removeIfExists(c('DISP','SPOSCAR'))
  
  #Read lines of POSCAR into Memory
  poscar     = readLines('POSCAR')
  
  #Backup the Original POSCAR file
  file.copy('POSCAR','POSCAR.bup')
  
  #Extract the Species, and Hold in Memory
  species    = poscar[6];
  
  #Remove the species names and write back to POSCAR, to enable phon to run without problem
  poscar     = poscar[-6]; writeLines(poscar,"POSCAR")
  
  #Run the Phon Software
  system(sprintf("%s%s%s",PHON_EXE[1],if(is.character(PHON_LOG[1]) & PHON_LOG[1] != ''){' > '}else{''},PHON_LOG[1]))
  
  #Post Process, OUTPUT should produce DISP and SPOSCAR files if Successful
  if(!file.exists("DISP") | !file.exists("SPOSCAR")){ stop("Error running the phon application",call.=F) }
  writeLines("Success Running phon, Post Processing Results")
  sposcar    = readLines('SPOSCAR')
  comment    = gsub("(\\s)+"," ",paste(poscar[1],sposcar[1])) 
  sposcar[1] = comment #Remove Whitespace via Regex
  sposcar    = c(sposcar[1:5],species,sposcar[6:length(sposcar)])          #Insert the Species Names Back In.
  writeLines(sposcar,'SPOSCAR')                                            #Write the SPOSCAR to File
  fileCopied = file.copy('SPOSCAR','POSCAR',overwrite = TRUE)              #Copy SPOSCAR back to POSCAR (Original POSCAR will be restored later)
  if(!fileCopied){ stop("Problem Coping SPOSCAR file to POSCAR",call.=F) } #Terminate if the copy action was problematic
  
  #Read the Diplsplacements
  df.disp           = read.table("DISP")
  df.disp           = sapply(df.disp,function(r){gsub('\\','',r,fixed=TRUE)})
  df.disp           = read.table(textConnection(df.disp));
  colnames(df.disp) = c("AtomID","x","y","z"); n = nrow(df.disp);
  df.disp.pos       = df.disp[(1:(n/2)),];    df.disp.pos$Side = "P"
  df.disp.neg       = df.disp[((n/2)+1):n,];  df.disp.neg$Side = "N"
  
  #Build the Displacement Schedule
  if(INCLUDE_POS & INCLUDE_NEG){
    writeLines("Including Positive and Negative")
    df.disp         = rbind(df.disp.pos,df.disp.neg)
  }else if(INCLUDE_POS){
    writeLines("Including Positive Only")
    df.disp         = df.disp.pos
  }else{
    writeLines("Including Negative Only")
    df.disp         = df.disp.neg
  }
  df.disp$Side = factor(df.disp$Side,levels=c("P","N")); rownames(df.disp) = 1:nrow(df.disp)
  df.disp <- ddply(df.disp,c("Side","AtomID"),function(df){ data.frame(Vec=1:nrow(df),df[,c("x","y","z")]) })
  rm(df.disp.pos,df.disp.neg)
  
  #Get the Present Working Directory
  presentDirectory = getwd()
  
  #Determine the Number of Characters required for fixed width numbers
  characters.ixs  = nchar(as.character(nrow(df.disp)))
  characters.atm  = nchar(as.character(max(df.disp$AtomID)))
  
  #Iterate over each Row of the Displacement Schedule, Producing a new simulation set
  df.disp$TargetDirectory = ""
  for(r in 1:nrow(df.disp)){
    #Determine the Parameters for this iteration
    atomID = df.disp[r,"AtomID"]
    side   = levels(df.disp$Side)[df.disp[r,"Side"]]
    dX     = df.disp[r,"x"]; dY = df.disp[r,"y"]; dZ = df.disp[r,"z"]
    if(verbose){
      fmt    = sprintf("DispID: %%%ii, AtomID: %%%ii, [%%+.4f,%%+.4f,%%+.4f]",characters.ixs,characters.atm)
      writeLines(sprintf(fmt,r,atomID,dX,dY,dZ))
    }
    
    #Create the New Directory
    targetDirectory = sprintf(sprintf("./%s/DISP_%%s_%%0%ii/",baseDirectory,characters.ixs),side,r)
    df.disp[r,"TargetDirectory"] = targetDirectory
    if(!file.exists(targetDirectory)){ dir.create(targetDirectory,recursive = TRUE) }
    
    #Copy the Static Files into the New Directory
    for(f in c("POSCAR","INCAR","KPOINTS","POTCAR","PBS")){ 
      if(file.exists(f)){
        file.copy(f,sprintf("%s/%s",targetDirectory,f),overwrite = TRUE) 
      }else{
        stop(sprintf("Required File (%s) is Missing",f),call.=F) #SHOULDN'T BE NEEDED, CHECKS RUN PERVIOUSLY
      }
    }
    
    #Step into the New Directory
    setwd(sprintf("%s/%s",presentDirectory,targetDirectory))
    
    #Write the PBS File, Change the Heading Tag
    pbs    = readLines("PBS")
    srch   = grep("#PBS -N",pbs)
    if(length(srch) >= 1){
      pbs[srch[1]] = sprintf(sprintf("#PBS -N %%s_P_%%s%%0%ii",characters.ixs),ALLOY,side,r)
      writeLines(pbs,"PBS")
    }
    
    #Create the Displacement Vector
    vector             = c(dX,dY,dZ)
    
    #Read the POSCAR File into memory
    poscar.disp        = readLines("POSCAR")
    
    #Modify the Comment Line
    poscar.disp[1]     = sprintf("%s AtomID %i, [dX=%+.5f,dY=%+.5f,dZ=%+.5f]",poscar.disp[1],r,vector[1],vector[2],vector[3])
    
    #Read the Positions
    positions          = read.table(textConnection(poscar.disp[START_ROW_POS:length(poscar.disp)]))
    
    #Apply the Displacement for AtomID
    positions[atomID,] = positions[atomID,] + vector 
    
    #Apply the changes made back to the vector of POSCAR lines, to be written to file.
    fmt                = sprintf("%% .%if",getOption('digits'))  #Dynamic Format Depending on Desired Number of Digits
    fmt                = sprintf("  %s %s %s",fmt,fmt,fmt)       #Produce the x,y,z line data
    for(rb in 1:nrow(positions)){ poscar.disp[START_ROW_POS + rb - 1] = sprintf(fmt,positions[rb,1],positions[rb,2],positions[rb,3]) }
    
    #Write the Newly Displaced POSCAR lines to File
    writeLines(poscar.disp,"POSCAR")
    
    #Step back to original directory
    setwd(presentDirectory)
  }
  
  #Add Row Numbers
  df.disp = data.frame(No=c(1:nrow(df.disp)),df.disp)
  
  #Write the Schedule to Disc
  #write.table(df.disp,sprintf("./%s/phon_disp_sched",baseDirectory),quote=F,row.names=F,col.names=T)
  write.table(df.disp,scheduleFile,quote=F,row.names=F,col.names=T)
  
  #Move the logfile to the batch directory
  if(file.exists(PHON_LOG)){
    file.copy(PHON_LOG,sprintf("./%s/%s",baseDirectory,PHON_LOG));
    removeIfExists(PHON_LOG)
  }
}
#Function to produce the force file
doForceFile <- function(){
  if(!file.exists(scheduleFile)) stop(sprintf("Schedule file, '%s', doesn't exist",scheduleFile),call.=FALSE) 
  df.sched = read.table(scheduleFile,header = TRUE)
  nDisp = nrow(df.sched)
  nAtom = length(unique(df.sched$AtomID))
  write(sprintf("%i",nDisp),file=forceFile,append=FALSE)
  for(ix in 1:nDisp){
    nSpec = df.sched[ix,"AtomID"]
    x     = df.sched[ix,"x"]
    y     = df.sched[ix,"y"]
    z     = df.sched[ix,"z"]
    write(sprintf("%i\t%.8f\t%.8f\t%.8f",nSpec,x,y,z),file=forceFile,append=TRUE)
    tDir  = df.sched[ix,"TargetDirectory"]
    outcar= sprintf("%s/OUTCAR",tDir); outcar = gsub("//","/",outcar)
    if(!file.exists(outcar)){ 
      writeLines(sprintf("'%s' file doesn't exist",outcar))
      write("<ERROR, MISSING DATA>",file=forceFile,append=TRUE)
    }else{
      writeLines(sprintf("Reading Lines, File %i/%i, from '%s'",ix,nDisp,outcar))
      lines = readLines(outcar)  
      start = grep("POSITION",lines)
      if(length(start) > 0){
        forces = read.table(textConnection(lines[(1:nAtom) + start + 1]),header=FALSE)[,4:6]
        for(ixB in 1:nrow(forces)){
          xx = forces[ixB,1]
          yy = forces[ixB,2]
          zz = forces[ixB,3]
          write(sprintf("%.8f\t%.8f\t%.8f",xx,yy,zz),file=forceFile,append=TRUE)
        }
      }
    }
  } 
}

#Try And Run
tryCatch({
  
  #Produce the Displacement Batches
  if(DISPLACEMENTS) { doDisplacements() }
  
  #Produce the Force File
  if(FORCEFILE){ doForceFile() }
  
#Error Handling
},exception=function(e){
  writeLines(as.character(e))
  
#Execute on Termination
},finally = {
  if(file.exists('POSCAR.bup')){ file.rename('POSCAR.bup','POSCAR') }
  removeIfExists('SPOSCAR'); removeIfExists('DISP')
})


