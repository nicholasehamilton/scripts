#! /bin/bash
#--------------------------------------
# ROUTINE FOR DETERMINING THE GROUND STATE
# LATTICE VECTOR FOR FCC MATERIAL
# Takes 2 arguments, 1 is mandatory:
# 1st arg lattice parameter a
# 2nd arg Name of Species, for Comment (Default X)
# Attempts to Build KPOINTS File Automatically if Specified Divisor > 0
#
# Date:   14th May 2015
# Author: Nicholas Hamilton
# Email:  n.hamilton@unsw.edu.au
#--------------------------------------

#Check the lattice vector value has been provided
if [ $# -lt 1 ]; then
	echo "Please Provide the Central Lattice Vector Value (1st Argument)"
	exit 0
fi

#VARIABLES
BIN="mpirun vasp"			# The Executable
CENTER=$1  				# Center Lattice Vector Value
dSTEP=0.1   				# Check Either Side in Steps of...
NSTEP=5        				# Number of Steps Either Side
SPEC=${2:-X}				# The Default Species Name
OUTPUT=SUMMARY.fcc 			# The Output File
KPOINTDIVISOR=11			# The Target Division in KPOINTS
BINKPOINTS=script_kpoints_automatic.R	# The Script to Build the KPOINTS FILE

#Determine the Steps
VALS=$(awk -v c="$CENTER" -v n="$NSTEP" -v s="$dSTEP" 'BEGIN{for(i=c-n*s;i<=c+n*s;i+=s)print i}')

#Remove Wavcar file if it exists
if [ -f WAVCAR ]; then rm WAVECAR; fi

#Count the Successes and Failures
CSUC=0; CTOT=0

#Process the loop for each lattice vector
for i in $VALS ; do
echo "Processing Lattice Vector: a= $i" 

#Build the POSCAR
cat > POSCAR <<!
$SPEC fcc:
$i
0.5 0.5 0.0
0.0 0.5 0.5
0.5 0.0 0.5
$SPEC
1
Cartesian
0 0 0
!

#Build the KPOINTS File
if [ "$KPOINTDIVISOR" -gt "0" ]; then
	$BINKPOINTS -i POSCAR -o KPOINTS -d $KPOINTDIVISOR
fi

#Check KPOINTSS FILE EXISTS (Either Provided or Built)
if [ ! -f KPOINTS ]; then
        echo "KPOINTS File Doesn't Exist, Terminating..."
        exit 0
fi

#Run the Executable
$BIN

#Determine the Result and report
touch $OUTPUT
if [ -f OSZICAR ]; then
	E=$(tail -1 OSZICAR) 
	echo $i $E >> $OUTPUT
	CSUC=$(($CSUC + 1))
fi
CTOT=$(($CTOT + 1))
done

#Report the Output File for Further Processing
if [ $CSUC -gt 0 ]; then
	echo Number of Values Successfully Processed: $CSUC/$CTOT
	cat $OUTPUT
else
	echo All $CTOT Trials Failed to Process
fi
exit 1
