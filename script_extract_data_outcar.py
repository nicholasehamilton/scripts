#!/usr/bin/env python
#--------------------------------------
# Script for extracting data from OUTCAR
# in tabular form, handles the case when
# OUTCAR has been concatinated from many
# Constituent OUTCAR files...
#
# Date:   8th July 2015
# Author: Nicholas Hamilton
# Email:  n.hamilton@unsw.edu.au
#--------------------------------------
import re, os, sys
import numpy     as np
import pandas    as pd
import optparse  as op

#The List of Valid Types
VALIDTYPES  = ['temperature','volume','pressure','stress','energy','cellvector','cellvectorreciprocal','force']

#Create optionparser and define usage
parser = op.OptionParser(usage='Usage: %prog <options>')

#Create the options
parser.add_option("-i", "--input",
		  dest="input", default='OUTCAR',
		  help="Input filename, an error will be throwin if file doesn't exist [Deafult '%default']")
parser.add_option("-o", "--output",
		  dest="output", default='',
		  help="Output filename, if blank, result will be printed to stdout/console [Deafult '%default']")
parser.add_option("--spliton",
		  dest="spliton", default=r'.*vasp.*build.*',
		  help="split the file at these major regex matches and process each chunk individually [Deafult '%default']")
parser.add_option("--type",
		  dest="type", default=VALIDTYPES[0],
		  help="Data to extract, valid values include '%s' [Deafult '%%default']" % "','".join(VALIDTYPES))
parser.add_option("--valuesonly",
                  action="store_true", dest="valuesonly", default=False,
                  help="Export Values Only, Drop Indexes [Deafult '%default']")
parser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="Verbose reporting to stdout/console [Deafult '%default']")

#Parse the Arguments
options, remainder = parser.parse_args()

#Load the Variables
INPUT   	= options.input
OUTPUT  	= options.output
SPLITON 	= options.spliton
TYPE    	= options.type
VERBOSE 	= options.verbose
VALUESONLY      = options.valuesonly

#RUN SOME CHECKS
if not os.path.isfile(INPUT):
	parser.print_help()
	sys.exit("ERROR: File '%s' Doesn't Exist" % INPUT)
if TYPE not in VALIDTYPES:
	parser.print_help()
	sys.exit("ERROR: Type '%s' Is Not Valid, valid values include '%s'" % (TYPE,"','".join(VALIDTYPES)))

#Load the Data
def readLinesFromFile():
	if VERBOSE:
		print "Attempting to Read Lines from File"
	with open(INPUT) as f:
		return f.readlines()

#Function to split vector of strings into vector of vectors, based on regex match.
def splitOn(lines,pattern=SPLITON):
	if VERBOSE:
		print "Attempting to Split Input File"
	p = re.compile(pattern)
	o = []
	for l in lines:
		if p.match(l):
			o.append([l])
		elif len(o) > 0:
			o[-1].append(l)
	return o

#Read and Split Lines	
lines      = readLinesFromFile()
linesSplit = splitOn(lines)

#Function to search file or character array based on regex match
#Includes 'offset' argument, which shifts the returned value by 'offset' lines
#to the line matching the regex pattern
def grep(pattern,fileObj,offset=0,returnIndexOnly=False):
	if VERBOSE:
		print "Attempting to match REGEX pattern '%s'" % pattern
	p = re.compile(pattern)
	indexes = [ (key + offset) for key,value in enumerate(fileObj) if p.match(value) ]
	return indexes if returnIndexOnly else [ fileObj[i] for i in indexes ]

#Function to extract data based on split
def extract(lines,position):
	if VERBOSE:
		print "Attempting to Extract Data"
	ret = [ [line.split()[p] for p in position] for line in lines]
	nPos = len(position)
	nVal = len(ret)
	ret  = [[ret[ixVal][ixPos] for ixVal in range(nVal)] for ixPos in range(nPos)]
	return ret	

#Put Values into Data Frame
def toDataFrame(groupid,values,propertyName,trimOffFront=0,trimOffEnd=0):
	nRow = len(values[0])
	if VERBOSE:
		print "Attempting to Process GroupID %i" % groupid
	df = pd.DataFrame({
		'GroupID'             : [groupid]*nRow,
		'%sID' % propertyName : [-1]*nRow
	})
	for ixVal in range(len(values)):
		df['%s%i' % (propertyName,ixVal+1) ] = values[ixVal]
	if trimOffFront > 0 and trimOffFront < len(df):
		df = df[trimOffFront:]
	if trimOffEnd   > 0 and trimOffEnd   < len(df):
		df = df[0:(len(df) - 1 - trimOffEnd)]
	df['%sID' % propertyName] = range(1,len(df)+1)
	return df

#Process Bathches for Regex Pattern
def process(pattern,position,propertyName='Value',offset=0,trimOffFront=0,trimOffEnd=0):
	if isinstance(position,list) == False:
		position = [position]
	numberOfGroups = len(linesSplit)
	if numberOfGroups > 0:
		frames   = [ toDataFrame(i+1,extract(grep(pattern,linesSplit[i],offset),position),propertyName,trimOffFront,trimOffEnd) for i in range(numberOfGroups)]
		df       = pd.concat(frames)
		df['ID'] = range(1,len(df)+1)
		df.index = df['ID']
		colIX    = ['ID','GroupID','%sID' % propertyName] + ["%s%i" % (propertyName,ixVal+1) for ixVal in range(len(position))]
		df       = df[colIX]
		if len(position) == 1:
			df.rename(columns={"%s1" % propertyName:propertyName}, inplace=True)
		return df
	else:
		sys.exit("ERROR: Illegal Number of Groups, must be > 0")
		
#Function to Pring Pandas Dataframe to file or console		
def export(df,fileName=None,sep=' '):
	if VALUESONLY:
		df = df.drop(df.columns[[0,1,2]],axis=1) 
	if isinstance(fileName,basestring) and fileName != '' and fileName is not None:
		if VERBOSE:
			print "Attempting to Export to File '%s'" % fileName
		df.to_csv(fileName,sep=sep,index=False)
	else:
		if VERBOSE:
			print "Attempting to Print Result to Console"
		pd.set_option('display.max_rows',	len(df) + 1)
		pd.set_option('display.max_columns',	len(df.columns) + 1)
		pd.set_option('display.width',          10000)
		print df.to_csv(sep=sep,   index=False)

#-------------------------------------------------------------------------------------
#Process / Extract Data for the Specific Type
#-------------------------------------------------------------------------------------
if   TYPE == 'temperature':
	result = process('.*\(temperature',       5,      propertyName='Temperature')
elif TYPE == 'energy':
	result = process('.*ion-electron.*TOTEN', 4,      propertyName='Energy')
elif TYPE == 'pressure':
	result = process('.*external pressure.*', 3,      propertyName='Pressure')
elif TYPE == 'stress':
	result = process('.*Pullay stress.*',     8,      propertyName='Stress')
elif TYPE == 'volume':
	result = process('.*volume of cell.*',    4,      propertyName='Volume',trimOffFront=1)
elif TYPE == 'cellvector' or TYPE == 'cellvectorreciprocal':
	propertyName = 'Total'  if TYPE == 'cellvector' else 'TotalReciprocal'
	theRange     = range(3) if TYPE == 'cellvector' else range(3,6)
	result       = process('.*length of vectors.*',theRange,propertyName=propertyName, trimOffFront=1,offset=1)
	components   = ["i","j","k"]
	for ixA in range(len(components)):
		propertyName = 'comp'
		dfComp       = process('.*direct lattice vectors.*',theRange,propertyName=propertyName,trimOffFront=1,offset=ixA+1)
		for ixB in [1,2,3]:
			result["%s%i" % (components[ixA],ixB)] = dfComp["%s%i" % (propertyName,ixB)]
elif TYPE == 'force':
	propertyName = 'Force'
	result = process('.*FORCE on cell.*', range(1,7),propertyName=propertyName,offset=13)
	result.rename(columns={ "%s1" % propertyName:'XX',
				"%s2" % propertyName:'YY',
				"%s3" % propertyName:'ZZ',
				"%s4" % propertyName:'XY',
				"%s5" % propertyName:'YZ',
				"%s6" % propertyName:'ZX'}, inplace=True)
else:
	sys.exit("ERROR: Value Type '%s' Not Recognised" % INPUT)

#Print the DataFrame to Console
export(result,OUTPUT)



